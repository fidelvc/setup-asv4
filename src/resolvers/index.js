export const resolvers = {
    Query: {
        testStatus: (parent, args, context, info)=> {
            return {
                status: "Test Ok"
            }
        },
        users: (parent, args, context, info) => {
            return [
                {
                    id: '01',
                    name: 'Root',
                    email: 'example@example.com',
                    password: '1234',
                    avatar: 'void'
                },
            ]
        }
    }
}