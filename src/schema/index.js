export const typeDefs = `#graphql

type Query {
    testStatus: TestStatus
    users: [User!]!
}

type TestStatus {
    status: String!
}
type User {
    id: ID!
    name: String!
    email: String!
    password: String!
    avatar: String
}

`;